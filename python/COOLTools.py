import sys
from PyCool import cool
import PlotTools as tools
import datetime as dt
import time,calendar

class RunInfoReader(object):
    def __init__(self):
        self.db = None
        self.opendb()

        self.eor = self.db.getFolder("/TDAQ/RunCtrl/EOR")
        self.sor = self.db.getFolder("/TDAQ/RunCtrl/SOR")
        self.ready = self.db.getFolder("/TDAQ/RunCtrl/DataTakingMode")
        self.lumi = self.db.getFolder("/TDAQ/OLC/LUMINOSITY")

    def closedb(self):
        self.db.closeDatabase()        
        
    def opendb(self):
        dbsvc = cool.DatabaseSvcFactory.databaseService()
        self.db = dbsvc.openDatabase("COOLONL_TDAQ/CONDBR2")

    def open_other_db(self):
        dbsvc = cool.DatabaseSvcFactory.databaseService()
        self.other_db = dbsvc.openDatabase("COOLONL_TRIGGER/CONDBR2")
                                
    def started(self, runnr):
        iov = runnr << 32
        objs = self.sor.countObjects(iov, iov, cool.ChannelSelection.all())
        return (objs > 0)

    def isready(self, runnr):
        iov = runnr << 32
        print (self.ready)
        objs = self.ready.countObjects(iov, iov, cool.ChannelSelection.all())
        print (objs)
        return (objs > 0)
    
    def ended(self, runnr):
        iov = runnr << 32
        objs = self.eor.countObjects(iov, iov, cool.ChannelSelection.all())
        return (objs > 0)
    
    def startstoptime(self, runnr):
        iov = runnr << 32
        objs = self.eor.browseObjects(iov, iov, cool.ChannelSelection.all())
        objs = [o for o in objs]
        try:
            return long(objs[0].payloadValue('SORTime')), \
                   long(objs[0].payloadValue('EORTime'))
        except IndexError:
            return None, None

    def readyLBs(self, runnr):
        since_iov = (runnr << 32) + 1
        till_iov = ((runnr + 1) << 32) - 1
        itr = self.ready.browseObjects(since_iov, till_iov, cool.ChannelSelection.all()) 
        #All possible LBs
        start_lb = 0; end_lb = 0
        while itr.goToNext():
            obj=itr.currentRef()
            payload=obj.payload()
            lb = obj.since() & 0xFFFFFFFF
            is_ready = payload['ReadyForPhysics']
            if is_ready and start_lb == 0:
                start_lb = lb
            if start_lb > 0 and end_lb == 0 and not is_ready:
                end_lb = lb
                break
        if start_lb > end_lb:
            print ('WARNING: Start LB ', start_lb, 'is greater than end LB',end_lb,'. Returning 0,0.')
            return 0,0
        return start_lb, end_lb

class LBParams:
     """Class to hold information from the LB_Params folder"""
     def __init__(self,run,lb,start,stop):
         self.run=run
         self.lb=lb
         self.start=start
         self.stop=stop
class RunParams:
    def __init__(self,run,lb_start,lb_end,start,stop):
        self.run = run
        self.lb_start = lb_start
        self.lb_end = lb_end 
        self.start = start
        self.stop = stop

def timeVal(val):
     "Convert argument to time in seconds, treating as a literal or date"
     try:
         a=int(val)
         return a
     except ValueError:
         try:
             ts=time.strptime(val+'/UTC','%Y-%m-%d:%H:%M:%S/%Z')
             return int(calendar.timegm(ts))
         except ValueError:
             print ("ERROR in time specification, use e.g. 2007-05-25:14:01:00")
             sys.exit(-1)


def find_runs(reader, since_str, till_str):
    since = timeVal(since_str)
    till = timeVal(till_str)
    if (till - since) > 60.*60.*24.*7:
        raise Exception("Trying to retrieve more than a week's worth of data. Why are you doing this to yourself...")
    itr = reader.lumi.browseObjects(since*1000000000,till*1000000000,cool.ChannelSelection.all())#,'OflLumiAcct-001')
    runlist = {}
    first_run = -1
    while itr.goToNext():
        obj=itr.currentRef()
        if obj.until() < cool.ValidityKeyMax or obj.since() >= cool.ValidityKeyMin:
           payload = obj.payload()
           run = payload['RunLB'] >> 32
           lb = payload['RunLB']&0xFFFFFFFF
           startLB=0; endLB=0; sstart = 0; send = 0
           if (run!=first_run):
               startLB, endLB = reader.readyLBs(run)
               #make sure we don't ask for lbs outside the time window
               #should change this later to include full runs but ok for now
               if startLB < lb: startLB = lb
               first_run=run
               runlist[run]=RunParams(run,startLB,endLB,sstart,send)
           if lb == runlist[run].lb_start:
               runlist[run].start = obj.since()
           elif lb > runlist[run].lb_start and lb <= runlist[run].lb_end and runlist[run].lb_start > 0:
               runlist[run].stop = obj.until()
               #incase atlas ends the run in ready, store a timestamp for the end! 

    runlist_ready = {}       
    for run in runlist:
        print (run, runlist[run].lb_start, '(',runlist[run].start,')', runlist[run].lb_end, '(',runlist[run].stop,')')
        if runlist[run].lb_start == 0 or runlist[run].lb_end == 0:
            print ('not keeping run ', run, ' because lb_end == 0 or lb_start == 0')
            continue
        if runlist[run].lb_end <= runlist[run].lb_start:
            print ('not keeping run ', run, ' because lb_end < lb_start')
            continue
        if runlist[run].start == 0 or runlist[run].stop ==0:
            print ('not keeping run ', run, ' because it has a zero timestamp (ready part of run probably falls outside requested time range)')
            continue
        runlist_ready[run] = runlist[run]
        print  ('Keeping run', run)
    return runlist_ready

def retrieve_ready_rates(reader, runlist, dsint = 200):
    outputs = []
    for run in runlist:
        sor = dt.datetime.fromtimestamp(runlist[run].start/1000000000)
        eor = dt.datetime.fromtimestamp(runlist[run].stop/1000000000)
        outputs += [tools.TRPTimepoints(sor, eor, downsample_interval=dsint, run_number = run)]
        print ('Retrieved run ',run,' in ready LB range ',runlist[run].lb_start, '-',runlist[run].lb_end )
    return outputs

def retrieve_run(reader, run_number, doReady = False):
    begin, end = reader.startstoptime(run_number)
    sor = dt.datetime.fromtimestamp(begin/1000000000)
    eor = dt.datetime.fromtimestamp(end/1000000000)

    if doReady:
        runlist = find_runs(reader, sor.strftime("%Y-%m-%d:%H:%M:%S"), eor.strftime("%Y-%m-%d:%H:%M:%S"))
        sor = dt.datetime.fromtimestamp(runlist[run_number].start/1000000000)
        eor = dt.datetime.fromtimestamp(runlist[run_number].stop/1000000000)

    return sor, eor

def retrieve_rates_for_run(reader, run_number, last=None, first=None, dsint=200, downsample_interval = 200, doReady=True):
    sor, eor = retrieve_run(reader, run_number,doReady=doReady)
    if dsint != 200:
        downsample_interval = dsint
    if first:
        eor = sor + dt.timedelta(0, first * 60)
    if last:
        sor = eor - dt.timedelta(0, last * 60)
    output = tools.TRPTimepoints(sor, eor, downsample_interval=downsample_interval, run_number = run_number)
    print ('retrieved run', str(run_number), ' between times ', sor, eor)
    return output
