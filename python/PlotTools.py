import datetime as dt
import matplotlib.pyplot as plt
import numpy as np
from beauty import Beauty
beauty = Beauty('https://atlasop.cern.ch')#http://pc-tbed-onl-01:8080')#'https://atlasop.cern.ch')#'http://pc-tdq-bst-02.cern.ch:8080')#'http://pc-tbed-onl-01:8080')
from pandas.core.resample import DatetimeIndex


class TRPTimepoints:
    """
    Retrieve the data for one or moreTimePoint_IS object from PBEAST.
    Provide methods to retrieve the desired rows and columns.
    """
    import datetime as dt
    import matplotlib.pyplot as plt
    import numpy as np

    
    def __init__(self, since, till, 
                 is_names = 'L1_Rate|HLT_Rate', 
                 partition = 'ATLAS', 
                 default_columns = { 'L1_Rate': 'TAP', 'HLT_Rate':'output'}, 
                 trp_server = 'ISS_TRP',
                 downsample_interval=0,
                 run_number = 999999):
        """
        Load the data and the metadata like x and y labels.
        """
        self.partition   = partition
        self.since       = since
        self.till        = till
        self.is_name     = trp_server + '.(' + is_names + ')'
        self.column_names = default_columns
        self.trp_server  = trp_server
        self.rows    = {}
        self.columns = {}
        self.data    = {}
        self.run_number = run_number
         
        # Get column and row names
        till_1       = since + dt.timedelta(0,60)

        # Get the data, data is an array of returned objects        
        for d in beauty.timeseries(since, till, partition, 'TimePoint_IS', 'Data', self.is_name, downsample_interval=downsample_interval):
            name = d.name.lstrip(trp_server+'.')
            self.data[name]    = d
            self.rows[name]    = beauty.timeseries(since, till_1, partition, 'TimePoint_IS', 'XLabels', d.name)[0].ys.T[0].tolist()                                
            self.columns[name] = beauty.timeseries(since, till_1, partition, 'TimePoint_IS', 'YLabels', d.name)[0].ys.T[0].tolist()
            
        self.pileup = beauty.timeseries(since, till, 'OLC', 'OCLumi', 'Mu', 'OLC.OLCApp/ATLAS_PREFERRED_LBAv_PHYS',downsample_interval=downsample_interval)[0].y
        self.lumi = beauty.timeseries(since, till, 'OLC', 'OCLumi', 'CalibLumi', 'OLC.OLCApp/ATLAS_PREFERRED_LBAv_PHYS', downsample_interval=downsample_interval)[0]

        self.livefraction = beauty.timeseries(since, till, 'ATLAS','CtpcoreTriggerRateInfo','live_fraction','L1CT.CTPCORE.Instantaneous.TriggerRates',downsample_interval=downsample_interval)[0]
        self.hltoutput = beauty.timeseries(since, till, 'ATLAS', 'SFOngCounters', 'WritingEventRate', 'DF.TopMIG-IS:HLT.Counters.Global',downsample_interval=downsample_interval)[0]
        try:
            self.mainRate = beauty.timeseries(since, till, 'ATLAS', 'SFOngCounters', 'WritingEventRate', 'DF.TopMIG-IS:HLT.Counters.physics_Main',downsample_interval=downsample_interval)[0]
            self.mainDataRate = beauty.timeseries(since, till, 'ATLAS', 'SFOngCounters', 'WritingDataRate', 'DF.TopMIG-IS:HLT.Counters.physics_Main',downsample_interval=downsample_interval)[0]
        except:
            print ('couldn\'t retrieve physics_Main - perhaps this stream was inactive this run')
            self.mainRate = []
            self.mainDataRate = []
        self.hltDataRate = beauty.timeseries(since, till, 'ATLAS', 'SFOngCounters', 'WritingDataRate', 'DF.TopMIG-IS:HLT.Counters.Global',downsample_interval=downsample_interval)[0]
        self.lb = beauty.timeseries(since, till, 'ATLAS', 'LumiBlock', 'LumiBlockNumber','RunParams.LumiBlock', downsample_interval = downsample_interval)[0]

    def get_data(self, name, row_name, column_name = None):
        """
        Retrieve the raw data for a given objetct row and column.
        Returns a tuple of(time[],data[])
        """
        if column_name is None:
            column_name = self.column_names[name]

        col = self.columns[name].index(column_name)
        row = self.rows[name].index(row_name)

        col_len = len(self.columns[name])
        data = self.data[name][row * col_len + col].y
        if column_name[-2:] == "ps":
            print ('Taking into account whether or not L1 items are disabled')
            ps_col = self.columns[name].index('PS')
            data = []
            for d, ps in zip(self.data[name][row * col_len + col].y,ps_col):
                if ps == -1:
                    data += [0]
                else:
                    data += [d]
    
        return self.data[name][row * col_len + col].x,data

    def default_column(self, name, column_name):
        """
        Set the default column to use. This avoids having to
        specify it every time when calling get_data() or plot()
        """
        self.column_names[name] = column_name
    
    def print_available_items(self, PrintHLTChains = True, PrintGroupsAndStreams = True, PrintL1Items = True):
        if PrintHLTChains:
            for item in self.rows['HLT_Rate']:
                if item[0:4] == 'HLT_':
                    print (item)
        if PrintGroupsAndStreams:
            for item in self.rows['HLT_Rate']:
                if item[0:4] != 'HLT_':
                    print (item )           
        if PrintL1Items:
            for item in self.rows['L1_Rate']:
                print (item)            

def plotL1PS(trp, L1Name = 'TAU20'):
    """
    Plot the L1Prescale of an item. Default is anything with TAU20 in it.
    """
    import matplotlib.pyplot as plt
    for trig in trp.rows['L1_Rate']:
        if L1Name in trig:
            ts_av, d_av = trp.get_data('L1_Rate',trig,'TBP')
            ts_ps, d_realps = trp.get_data('L1_Rate',trig,'PS')
            avg_dav = float(sum(d_av))/float(len(d_av))
            avg_ps = float(sum(d_realps)) / float(len(d_realps))
            d_av_new = []
            non_zero_values = 0
            for v, p in zip(d_av,d_realps):
                if p != 1:
                    d_av_new += [0]
                else:
                    d_av_new += [v]
                    non_zero_values += 1
            if non_zero_values > 0:
                avg_rate = float(sum(d_av_new)) / float(non_zero_values)
                plt.plot(ts_av, d_av_new, label=trig)
        
def plot(trp, name, row_name, column_name = None, label = None, color = 'black', linestyle = '--'):
    """
    Plot the data for a given row and column using matplotlib.
    Optionally specify a custom label for the plot, otherwise
    the row_name is used.
    Doesn't plot points where the physics_Main rate is < 500 Hz (for HLT) 
    or L1A rate is < 40kHZ (for L1 item).
    Has the option to exclude points with livefraction < X, 
    but now this is set to 0. 
    Using livefraction can cause odd features, because there is a time delay
    between the livefraction going down and the rate being affected.
    """
    if label is None:
        label = row_name
    
    timestamps, data = trp.get_data(name, row_name, column_name)
    ts2 = []; data2 = []; lf2 = []
    pm_ts = []; pm_data = []
    pm_ts, pm_data = trp.get_data('HLT_Rate','str_Main_physics',None)
    
    for i, lf_ts in enumerate(trp.lf.x):
        for j, data_ts in enumerate(timestamps):
            if lf_ts == data_ts:
                data2 += [data[j]]
                ts2 += [data_ts]
                lf2 += [trp.lf.y[i]]
    timestamps = ts2; data = data2
    ts2 = []; data2 = []
    iterator = 0
    for x,y,ypm,l in zip(timestamps,data,pm_data,lf2):
        iterator += 1
        if l < 0 or iterator < 6:
            continue
        if name == 'HLT_Rate' and ypm < 500:
            continue
        if name == 'L1_Rate' and y < 40000:
            continue
        else:
            ts2 += [x]
            data2 += [y]
    plt.plot(ts2, data2, label=label, color=colour,linewidth=2.0, linestyle=linestyle)
    
def plotStacked(trp, name, row_names, lf, column_name = None, colours = [], doRateRestrict = False):
    """
    Plot a stacked histogram for a list of row_names (trigger rates, group rates, or stream rates). 
    
    If doRateRestrict = True, doesn't plot points where the physics_Main rate is < 500 Hz (for HLT)
    or L1A rate is < 40kHZ (for L1 item).  This can help get rid of spikes in the plot without
    using the livefraction, which has slightly different timestamps.  
    """
    
    datas = []
    ts2 = []
    timestamps = []
    exclude_times = []
    str_test_stream = 'str_Main_physics'
    if name != 'HLT_Rate':
        str_test_stream = 'L1A'
    ts_main, d_main = trp.get_data(name, str_test_stream, column_name)
    
    if doRateRestrict:
        for iterator,(t,d) in enumerate(zip(ts_main, d_main)):
            if name == 'HLT_Rate' and d < 500:
                exclude_times += [t]
            if name == 'L1_Rate' and d < 40000:
                exclude_times += [t]
            if iterator < 8:
                exclude_times += [t]
            
    for row1 in row_names:
        datatmp = []
        for row_name in row1:
            print (row_name)
            ts2 = []; data2 = []; lf2 = []
            timestamps, data = trp.get_data(name, row_name, column_name)
            
            for i, lf_ts in enumerate(lf.x):
                for j, data_ts in enumerate(timestamps):
                    if lf_ts == data_ts and lf_ts not in exclude_times:
                        data2 += [data[j]]
                        ts2 += [data_ts]
                        lf2 += [lf.y[i]]
            timestamps = ts2; ts2 = []
            data = data2; data2 = []
            if len(datatmp) > 0:
                for x,y,l in zip(timestamps,data,lf2):
                    if l < 0:
                        continue
                    else:
                        ts2 += [x]
                        data2 += [y]
    
                datatmp = [x + y for x, y in zip(datatmp, data2)]
            else: 
                for x,y,l in zip(timestamps,data,lf2):
                    if l < 0:
                        continue
                    else:
                        ts2 += [x]
                        data2 += [y]
                datatmp = data2
        datas += [datatmp]
    fig, ax = plt.subplots(figsize=(10,6))
    ax.stackplot(DatetimeIndex(ts2), datas,edgecolor='white', colors = colours)
    return ax

def plotLines(trp, name, row_names, lf, column_name = None, colours = [], dash_styles=[], doRateRestrict = False):
    """
    Plots a list of inputs (groups, triggers, or streams) as lines with input colours and styles. 
    
    If doRateRestrict = True, doesn't plot points where the physics_Main rate is < 500 Hz (for HLT)
    or L1A rate is < 40kHZ (for L1 item).  
    """
    datas = []
    ts2 = []
    timestamps = []
    exclude_times = []
    str_test_stream = 'str_Main_physics'
    if name != 'HLT_Rate':
        str_test_stream = 'L1A'
    ts_main, d_main = trp.get_data(name, str_test_stream, column_name)
    
    if doRateRestrict:
        for iterator,(t,d) in enumerate(zip(ts_main, d_main)):
            if name == 'HLT_Rate' and d < 500:
                exclude_times += [t]
            if name == 'L1_Rate' and d < 40000:
                exclude_times += [t]
            if iterator < 8:
                exclude_times += [t]
                
    for row_name in row_names:
        ts2 = []; data2 = []; lf2 = []
        timestamps, data = trp.get_data(name, row_name, column_name)
            
        for i, lf_ts in enumerate(lf.x):
            for j, data_ts in enumerate(timestamps):
                if lf_ts == data_ts and lf_ts not in exclude_times:
                    data2 += [data[j]]
                    ts2 += [data_ts]
                    lf2 += [lf.y[i]]
        timestamps = ts2; ts2 = []
        data = data2; data2 = []

        for x,y,l in zip(timestamps,data,lf2):
            if l < 0:
                continue
            else:
                ts2 += [x]
                data2 += [y]
    
        datas += [data2]
        
    fig, ax = plt.subplots(figsize=(10,6))
    for d,c,ds in zip(datas,colours,dash_styles):
        ax.plot(DatetimeIndex(ts2), d,color = c,linewidth=3.0, dashes = ds)
    return ax

def plotRatio(trp, name, row_name1, row_name2, column_name = None, label = None, colour = 'blue', marker = 'o'):
    """
    Plot the ratio of two L1 items or two HLT items. 
    
    No attempt is made to remove points with large deadtime.
    
    If L1 items, choose TBP, TAP, or TAV. This will apply to both items.
    """
    if label is None:
        label = row_name1
            
    # private import
    import matplotlib.pyplot as plt
    
    timestamps, data = trp.get_data(name, row_name1, column_name)
    timestamps2, data2 = trp.get_data(name, row_name2, column_name)     
       
    plt.plot(timestamps, data/data2, label=label, color = colour, marker=marker)
    
def plotL1HLTRatio(trp, row_name1, row_name2, column_name = None, label = None, colour = 'blue', marker = 'o'):
    """
    Plot the ratio betwen HLT item row_name1 and L1 item row_name2. 
    
    The L1 column must be specified (TBP, TAP, or TAV).
    """
    if label is None:
        label = row_name1
            
    # private import
    import matplotlib.pyplot as plt
    
    timestamps, data = trp.get_data('HLT_Rate', row_name1, None)
    timestamps2, data2 = trp.get_data('L1_Rate', row_name2, column_name)
        
    plt.scatter(timestamps, data/data2, label=label, color = colour, marker=marker)
    
def plotXSvsPileup(trp, trig_level, row_name, column_name, label = None, colour = None, marker = 'o', doOnlyL1 = False,
                   unprescale = True, l1seed = '', doLinearFit = False, sig_remove = 0, threshold = 0, ax = None):
    """
    Plot the cross-section of a trigger (L1 or HLT) vs. <mu>.
    
    By default, unprescales the trigger, because prescaled cross-sections don't make much sense.
    
    Cross-section is defined as rate/livefraction/lumi. 
    
    doLinearFit: Adds linear fits that attempt to only fit points that are not outliers 
    with sigma > sig_remove. Can also remove points below some threshold (default = 0). Not perfect.
    """
    
    # private import
    import matplotlib.pyplot as plt
    
    if row_name == '' and l1seed == '':
        print ('what are you plotting? you need to provide a trigger name!')
        return 0,0
    
    if row_name == '' and not l1seed == '':
        doOnlyL1 = True
        row_name = 'HLT_j480' #just a dummy trigger that always runs so the code doesnt need as much changing...   
        print ('no HLT chain provided, plotting TBP for ',l1seed)
        
    if ((label == None or label == '') and doOnlyL1):
        label = l1seed
    elif label is None:
        label = row_name
            
    timestamps, data = trp.get_data(trig_level, row_name, column_name)
    lumi = trp.lumi.y
    
    pileup = trp.pileup
    lf =  []
    tsl1ap, datal1ap = trp.get_data('L1_Rate', l1seed, 'TAP')
    tsl1av, datal1av = trp.get_data('L1_Rate', l1seed, 'TAV')

    for ap, av in zip(datal1ap, datal1av):
        if ap > 0:
            lf += [av/ap]
        else:
            lf += [0.]

    tsps = None; dataps = None

    if unprescale and not doOnlyL1:
        if l1seed == '':
            print ('ERROR: Need to give plotXSvsPileup a L1 seed it you want to unprescale the item!')
            return 0,0
        tsps, dataps = trp.get_data(trig_level, row_name, 'prescaled')
        tsl1bp, datal1bp = trp.get_data('L1_Rate', l1seed, 'TBP')
    elif not unprescale and doOnlyL1:
        timestamps, data = trp.get_data('L1_Rate', l1seed, 'TAP')        
    elif unprescale and doOnlyL1:
        timestamps, data = trp.get_data('L1_Rate', l1seed, 'TBP')

     #Make sure all the arrays are the same length...

    #print len(data), len(lf), len(pileup),len(lumi)

    min_len = len(data)
    if len(pileup) < min_len:
        min_len = len(pileup)
    if len(lumi) < min_len:
        min_len = len(lumi)
    if len(lf) < min_len:
        min_len = len(lf)  
        
    if len(data) > min_len:
        data = data[0:min_len]
        if unprescale and not doOnlyL1:
            dataps = dataps[0:min_len]
            datal1bp = datal1bp[0:min_len]
            datal1ap = datal1ap[0:min_len]
    if len(pileup) > min_len:
        pileup= pileup[0:min_len]
    if len(lumi) > min_len:
        lumi = lumi[0:min_len]
    if len(lf) > min_len:
        lf = lf[0:min_len]
    
    pileup2 = []
    data2 = []
    
    #times 1000 to make it nb not microbarns (lumi comes in 10^-30 cm^-2 s^-1, which is microbarns / second)        
    if doOnlyL1:
        #don't need the livefraction for only L1 because we just take TBP/TAP :)
        for x,y,dl1 in zip(pileup,lumi,data):
            pileup2 += [x]
            data2 += [dl1/y*1000.]
    elif unprescale:
        for x,y,z,l,hltin,l1bp,l1ap in zip(pileup,lumi,data,lf,dataps,datal1bp,datal1ap):
            if l < 70 or z == 0 or hltin == 0:
                continue
            else:
                #print x,y,z,hltin,l1bp,l1ap
                l1_prescale = l1bp/l1ap
                hltps = l1ap/hltin
                pileup2 += [x]
                data2 += [hltps*l1_prescale*z/l/y*1000.]
    else:
        for x,y,z,l in zip(pileup,lumi,data,lf):
            if l < 0.7:
                continue
            else:
                pileup2 += [x]
                data2 += [z/l/y*1000.]
    if ax == None:
        plt.scatter(pileup2,data2,label=label,color=colour, marker=marker)
    else:
        ax.scatter(pileup2,data2,label=label,color=colour, marker=marker)
    #remove outliers removes all points where the rate is N standard deviations away from the mean.
    #Doesn't work well for long periods of time where the rate falls a lot!
    #This value can be tuned or removed altogether.
   
    m=0; b=0  
 
    if doLinearFit:
    
        pileup_new, data_new = remove_outliers(pileup2, data2, sig_remove, threshold)
    
        axes = plt.gca()
        m, b = np.polyfit(pileup_new,data_new,1)
        X_plot = np.linspace(axes.get_xlim()[0],axes.get_xlim()[1],100)

        plt.plot(X_plot, m*X_plot + b, '-', color=colour)

    return m,b    
    
def plotRatevsLumi_constrained(trp, trig_level, row_name, column_name, lumi, lf = [], color = None, label = None):
    """
    I don't remember what this constrains...
    """
    if label is None:
        label = row_name
            
    # private import
    import matplotlib.pyplot as plt
    
    timestamps, data = trp.get_data(trig_level, row_name, column_name)
    min_len = len(data)
    if len(lumi) < min_len:
        min_len = len(lumi)
    if len(lf) < min_len and len(lf) > 0:
        min_len = len(lf)
    
    if len(data) > min_len:
        data= data[0:min_len]
    if len(lumi) > min_len:
        lumi = lumi[0:min_len]
    if len(lf) > min_len and len(lf) > 0:
        lf = lf[0:min_len]
      
    lumi2 = lumi
    data2 = data
    if len(lf) > 0:
        lumi2, data2 = scale_by_livefraction(lumi, data, lf)
    
    lumi_new, data_new = remove_outliers(lumi2, data2, 4, 10)
    
    axes = plt.gca()
    lumi_new = np.array(lumi_new)
    lumi_new = lumi_new[:,np.newaxis]
    a, _, _, _ = np.linalg.lstsq(lumi_new, data_new)
    
    X_plot = np.linspace(axes.get_xlim()[0],axes.get_xlim()[1],100)

    plt.plot(X_plot, a*X_plot, '-', color=color)

    return a


def plotRatevsLumi(trp, trig_level, row_name, column_name, label = None, colour = 'blue', marker = 'o',
                   useLivefraction = True, doLinearFit = True, minimumRateForFit = 5, sig_remove = 4, scaleY = 1, scaleX = 1):
    """
    Plot the data for a given row and column using matplot.
    
    Optionally specify a custom colour, marker, label for the plot. 
    If no label is specified, the row_name is used as label.
    
    Default is to scale by the livefraction (e.g. rate = observed_rate/livefraction), 
    so the plot is not affected by detector deadtime. Should be true when comparing runs.
    Points with livefraction < 0.7 are removed.
    
    A linear fit can be performedo on the data. This does not work well for triggers that
    have a noisy rate, so points with rate > 'sig_remove' sigma from the average are removed from the fit
    (they still appear on the plot). You can also specify a rate below which to not consider
    points for the fit (minimumRateForFit). Default is 5 Hz.
    """

    if label is None:
        label = row_name
            
    # private import
    import matplotlib.pyplot as plt
    
    timestamps, data = trp.get_data(trig_level, row_name, column_name)
    
    lumi = trp.lumi
    lf = []
    if useLivefraction: 
        lf = trp.livefraction
    
    #Sometimes the vectors coming out of trp are slightly different lengths (beginning/end points can be left off).
    #Here we make sure they are the same size.
    min_len = len(data)
    if len(lumi) < min_len:
        min_len = len(lumi)
    if len(lf) < min_len and len(lf) > 0:
        min_len = len(lf)
    if len(data) > min_len:
        data= data[0:min_len]
    if len(lumi) > min_len:
        lumi = lumi[0:min_len]
    if len(lf) > min_len and len(lf) > 0:
        lf = lf[0:min_len]
      
    lumi2 = lumi
    data2 = data
    if len(lf) > 0:
        lumi2, data2 = scale_by_livefraction(lumi, data, lf)
    lumi2 = scaleX*lumi2
    data2 = scaleY*data2
    plt.scatter(lumi2,data2,label=label,color=colour, marker=marker)
    
    if doLinearFit:
        lumi_new, data_new = remove_outliers(lumi2, data2, sig_remove, minimumRateForFit)
        axes = plt.gca()
        m, b = np.polyfit(lumi_new,data_new,1)
        X_plot = np.linspace(axes.get_xlim()[0],axes.get_xlim()[1],100)
        plt.plot(X_plot, m*X_plot + b, '-', color=colour)
        return m,b
    else:
        return 0,0

def scale_by_livefraction(x_vec = [],y_vec = [], lf_vec = []):
    x_new = []
    y_new = []
    for x,y,lf in zip(x_vec,y_vec,lf_vec):
        if lf < 70:
            continue
        else:
            x_new += [x]
            y_new += [y*100./lf]
            
    return x_new, y_new
    
def remove_outliers(x_vec = [],y_vec = [], nstdev = 1, threshold = 0):
    x_new = []
    y_new = []
    avg = np.average(y_vec)
    stdev = np.std(y_vec)
    for x,y in zip(x_vec,y_vec):
        if abs(avg-y) > (stdev * nstdev):
            print ('removing point with rate ', y)
            continue
        elif y < threshold:
            continue
        else:
            x_new += [x]
            y_new += [y]
    return x_new, y_new
def plotBWvsLumi(trp, label = None, colour = 'blue', marker = 'o', scaleY = 1, scaleX = 1):
    """
    Plot the data for a given row and column using matplot.
    
    Optionally specify a custom colour, marker, label for the plot. 
    If no label is specified, the row_name is used as label.
    
    Default is to scale by the livefraction (e.g. rate = observed_rate/livefraction), 
    so the plot is not affected by detector deadtime. Should be true when comparing runs.
    Points with livefraction < 0.7 are removed.
    
    A linear fit can be performedo on the data. This does not work well for triggers that
    have a noisy rate, so points with rate > 'sig_remove' sigma from the average are removed from the fit
    (they still appear on the plot). You can also specify a rate below which to not consider
    points for the fit (minimumRateForFit). Default is 5 Hz.
    """

    if label is None:
        label = 'blahblahblah'
            
    # private import
    import matplotlib.pyplot as plt
    
    data = trp.hltDataRate
    
    lumi = trp.lumi
    
    #Sometimes the vectors coming out of trp are slightly different lengths (beginning/end points can be left off).
    #Here we make sure they are the same size.
    min_len = len(data)
    if len(lumi) < min_len:
        min_len = len(lumi)
    if len(data) > min_len:
        data= data[0:min_len]
    if len(lumi) > min_len:
        lumi = lumi[0:min_len]
      
    lumi2 = scaleX*lumi
    data2 = scaleY*data
    plt.scatter(lumi2,data2,label=label,color=colour, marker=marker)
    return

def plotFreeCoresvsLumi(trp, fc, label = None, colour = 'blue', marker = 'o', scaleY = 1, scaleX = 1):
    """
    Plot the data for a given row and column using matplot.
    
    Optionally specify a custom colour, marker, label for the plot. 
    If no label is specified, the row_name is used as label.
    """

    if label is None:
        label = 'blahblahblah'
            
    # private import
    import matplotlib.pyplot as plt
    
    data = fc.y
    
    lumi = trp.lumi
    
    #Sometimes the vectors coming out of trp are slightly different lengths (beginning/end points can be left off).
    #Here we make sure they are the same size.
    min_len = len(data)
    if len(lumi) < min_len:
        min_len = len(lumi)
    if len(data) > min_len:
        data= data[0:min_len]
    if len(lumi) > min_len:
        lumi = lumi[0:min_len]
      
    lumi2 = scaleX*lumi
    data2 = scaleY*data
    plt.scatter(lumi2,data2,label=label,color=colour, marker=marker)
    return
